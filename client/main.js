// setting
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import angularRoute from 'angular-route';
import '../imports/startup/accounts-config.js';
import uiRouter from 'angular-ui-router';

// template
import indexUrl from '/imports/index.html';
import layoutUrl from '/imports/layout.html';
import createNamecardUrl from '/imports/createNamecard.html';
import namecardListUrl from '/imports/namecardList.html';
import namecardViewUrl from '/imports/namecardView.html';
import createCategoryUrl from '/imports/createCategory.html';
import categoryListUrl from '/imports/categoryList.html';
import categoryListXsUrl from '/imports/categoryListXs.html';


// Mongo DB
import { Namecards } from '../imports/api/namecards.js';
import { Categorys } from '../imports/api/categorys.js';
import { Members } from '../imports/api/members.js';

angular.module('contactMgr', [angularMeteor, uiRouter, 'accounts.ui'])

.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state('index', {
            	url : "/",
            	views: {
            		'layout': {
            			templateUrl : indexUrl,
            			controller : 'indexCtrl'
            		}
            	}	
            })
            .state('list', {
                url: "/list?categoryId&namecardId",
                views: {
                	'layout': {
                		templateUrl: layoutUrl
                	},
                	'createCategory@list': { 
	            		templateUrl : createCategoryUrl,
	            		controller : 'categoryListCtrl' 
	            	},
                	'catogoryList@list': { 
	            		templateUrl : categoryListUrl,
	            		controller : 'categoryListCtrl' 
	            	},
	            	'categoryListXs@list': { 
	            		templateUrl : categoryListXsUrl,
	            		controller : 'categoryListXsCtrl' 
	            	},
	            	'createNamecard@list': { 
	            		templateUrl : createNamecardUrl,
	            		controller : 'namecardListCtrl' 
	            	},
	                'namecardList@list': { 
	                	templateUrl : namecardListUrl,
	                	controller : 'namecardListCtrl'
	                },
	                'namecardView@list': { 
	                	templateUrl : namecardViewUrl,
	                	controller : 'namecardViewCtrl'
	                }
                }
            })
    }
])

// 설정한 숫자 이상의 글자일때는 ... 처리
.filter('truncate', function(){
	return function(input, limit) {
		return (input.length > limit) ? input.substr(0, limit)+'...' : input;
	}
})
// 모달 창 디렉티브
.directive('modal', function () {
    return {
		template: '<div class="modal fade">' + 
					'<div class="modal-dialog">' + 
						'<div class="modal-content">' + 
							'<div class="modal-header">' + 
						 		'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
								'<h4 class="modal-title">{{ title }}</h4>' + 
							'</div>' + 
						'<div class="modal-body" ng-transclude></div>' + 
					'</div>' + 
					'</div>' + 
				  '</div>',
		restrict: 'E',
		transclude: true,
		replace:true,
		scope:true,
		link: function postLink(scope, element, attrs) {
			scope.title = attrs.title;

			scope.$watch(attrs.visible, function(value){
			  if(value == true)
			    $(element).modal('show');
			  else
			    $(element).modal('hide');
			});

			$(element).on('shown.bs.modal', function(){
			  scope.$apply(function(){
			    scope.$parent[attrs.visible] = true;
			  });
			});

			$(element).on('hidden.bs.modal', function(){
			  scope.$apply(function(){
			    scope.$parent[attrs.visible] = false;
			  });
			});
		}
	};
})
// 한글 입력 바인딩 오류 수정 디렉티브
.directive('krInput', [ '$parse', function($parse) {
	return {
	    priority : 2,
	    restrict : 'A',
	    compile : function(element) {
	        element.on('compositionstart', function(e) {
	            e.stopImmediatePropagation();
	        });
	    },
	}
}])
.controller('AppCtrl', function($scope, $meteor, $state, $rootScope, $stateParams) {
	$scope.subscribe('namecards');
	$scope.subscribe('categorys');

	// 로그인 여부 체크용 변수
	// $scope.isLogin = false;
	$scope.helpers({
		currentUser() {
			// 로그인을 했다면 유저id은 null이 아닌 값이 들어간다
			$scope.currentUserId = Meteor.userId();
        	return Meteor.user();
      	}
	})

	$scope.params = $stateParams;

	// 로그인 상태에 따른 state 이동 처리
    Tracker.autorun(function () {
	  var userId = Meteor.userId();
	  $scope.isLogin = false;
	  // 로그인 상태가 아니라면 index 화면으로 이동
	  if (!userId) {
	    $state.go("index");  // go 'home' on logout
	  // 로그인 상태라면 list 화면으로 이동
	  } else if (userId) {
	  	$state.go("list");
	  	$scope.isLogin = true;
	  }
	});

    // 로그인 하지 않으면 index 페이지 외엔 접근할 수 없도록 처리
	$rootScope.$on("$stateChangeSuccess", function() {
    	var userId = Meteor.userId();
    	if(!userId) {
    		$state.go("index");
    	}
    });
      
})

.controller('indexCtrl', function($scope, $state, $rootScope, $stateParams) {
	// $rootScope.$on("$stateChangeSuccess", function() {
 //    	console.log(Meteor.userId());
 //    	if(Meteor.userId()) {
 //    		$state.go("list");
 //    	}
 //    })
})

.controller('categoryListCtrl', function($scope, $rootScope, $stateParams) {
	$scope.showModal = false;
	$scope.mode = undefined;
	// 분류 폼 데이터
	$scope.categoryForm = {id: '', name: '', explanation: ''};
	// 분류 리스트 가져오기
	$scope.helpers({
		categorys() {
			var categorys = [];
			// 전체 분류는 id를 0으로 지정
			categorys.push({_id: "0", name: "ALL", explanation: "전체"})
			var allCategorys = Categorys.find({}).fetch();
			allCategorys.forEach(function(category) {
				categorys.push(category);
			})
			return categorys
		}
	});

	// if($scope.params.categoryId != undefined) {
	// 	$scope.selectCategory($scope.params.categoryId);
	// }

	// 분류 모달
    $scope.categoryModal = function(type, categoryObj){
    	if(type === 'modify') {
    		$scope.categoryForm.id = categoryObj._id;
    		$scope.categoryForm.name = categoryObj.name;
    		$scope.categoryForm.explanation = categoryObj.explanation;
    		$scope.mode = "modify";	
    	} else {
    		$scope.categoryForm.id = "";
    		$scope.categoryForm.name = "";
    		$scope.categoryForm.explanation = "";
    		$scope.mode = "add";
    	}
    	$scope.showModal = !$scope.showModal;

    };
    // 분류 폼 유효성 검사
	$scope.validation = function() {
		if($scope.categoryForm.name === undefined || $scope.categoryForm.name.trim().length === 0) {
			alert("분류명을 입력하세요");
		} else if($scope.categoryForm.explanation === undefined || $scope.categoryForm.explanation.trim().length === 0) {
			alert("설명을 입력하세요");
		} else {
			return true;
		}
	};
	// 분류 추가
	$scope.addCategory = function() {
		if($scope.validation()) {
			Categorys.insert({
				name: $scope.categoryForm.name,
				explanation: $scope.categoryForm.explanation,
				createdAt: new Date(),
				owner: Meteor.userId(),
				username: Meteor.user().username
			});
		}
		$scope.showModal = !$scope.showModal;
	};
	// 분류 수정
	$scope.modifyCategory = function() {
		if($scope.validation()) {
			Categorys.update($scope.categoryForm._id, {
				$set: {
					name: $scope.categoryForm.name,
					explanation: $scope.categoryForm.explanation
				},
			});
		}
		$scope.showModal = !$scope.showModal;
	};
	// 분류 삭제
	$scope.deleteCategory = function(id) {
		var rmNamecards = Namecards.find({category: id});
		var categoryCnt = Namecards.find({category: id}).count();
		if (categoryCnt > 0) {
			if(confirm("해당 분류에 "+categoryCnt+"개의 명함이 존재합니다. \n분류 삭제시 명함도 함께 삭제됩니다. 정말 삭제하시겠습니까??") == true) {
				rmNamecards.forEach(function(category) {
					Namecards.remove({_id: category._id});
				})
				Categorys.remove({_id: id});
			} else {

			}
		} else {
			if(confirm("정말 삭제하시겠습니까??") == true) {
				Categorys.remove({_id: id});

			} else {

			}
		}
	};

	$scope.selectCategory = function(id) {
		$scope.activeCategory = id;

		//선택한 분류id를 방송한다
		$rootScope.$broadcast('categoryId', id);
	}
})

.controller('categoryListXsCtrl', function($scope, $rootScope, $stateParams) {
	$scope.helpers({
		categorys() {
			var categorys = [];
			// 전체 분류는 id를 0으로 지정
			categorys.push({_id: "0", name: "ALL"})
			var allCategorys = Categorys.find({}).fetch();
			allCategorys.forEach(function(category) {
				categorys.push(category);
			})
			return categorys
		}
	});
	// 분류 선택
	$scope.selectCategoryId = function() {
		//카테고리id를 방송한다(xs사이즈 일 경우)
		$rootScope.$broadcast('selectNamecard', {categoryId: $scope.selectCategory});
	}
})

.controller('namecardListCtrl', function($scope, $rootScope, $stateParams) {
	//방송되는 분류id를 받는다
	$scope.$on('categoryId', function(evt, id) {
		if(id === "0") {
			$scope.namecards = Namecards.find({}).fetch();
		} else {
			$scope.categoryId = id;
			$scope.namecards = Namecards.find({category: $scope.categoryId}).fetch();	
		}
	})

	$scope.showModal = false;
	$scope.mode = undefined;
	// 명함 폼 데이터
	$scope.namecardForm = {id: '', category: '', department: '', position: '', name: '', email: '', phone: ''};
	// 분류 리스트 가져오기
	$scope.helpers({
		categorys: () => Categorys.find({})
	});

	// if($stateParams.namecardId != undefined) {
	// 	$scope.detailNamecard($stateParams.namecardId);
	// }

	// 명함 모달
    $scope.namecardModal = function(type, namecardObj){
    	if(type === 'modify') {
    		$scope.namecardForm.id = namecardObj._id;
    		$scope.namecardForm.category = namecardObj.category;
			$scope.namecardForm.department = namecardObj.department;
			$scope.namecardForm.position = namecardObj.position;
			$scope.namecardForm.name = namecardObj.name;
			$scope.namecardForm.email = namecardObj.email;
			$scope.namecardForm.phone = namecardObj.phone;
			$scope.namecardForm.address = namecardObj.address;
    		$scope.mode = "modify";	
    	} else {
    		$scope.namecardForm.id = "";
    		$scope.namecardForm.category = "";
			$scope.namecardForm.department = "";
			$scope.namecardForm.position = "";
			$scope.namecardForm.name = "";
			$scope.namecardForm.email = "";
			$scope.namecardForm.phone = "";
			$scope.namecardForm.address = "";
    		$scope.mode = "add";
    	}
    	$scope.showModal = !$scope.showModal;

    };

    // 명함 폼 유효성 검사
	$scope.validation = function () {
		if($scope.namecardForm.category === undefined || $scope.namecardForm.category.trim().length === 0) {
			alert("분류를 선택하세요.");
		} else if($scope.namecardForm.department === undefined || $scope.namecardForm.department.trim().length === 0) {
			alert("부서를 입력하세요.");
		} else if($scope.namecardForm.position === undefined || $scope.namecardForm.position.trim().length === 0) {
			alert("직급을 입력하세요.");
		} else if($scope.namecardForm.name === undefined || $scope.namecardForm.name.trim().length === 0) {
			alert("이름을 입력하세요.");
		} else if($scope.namecardForm.email === undefined || $scope.namecardForm.email.trim().length === 0) {
			alert("이메일주소를 입력하세요.");
		} else if($scope.namecardForm.phone === undefined || $scope.namecardForm.phone.trim().length === 0) {
			alert("핸드폰번호를 입력하세요.");
		} else if($scope.namecardForm.address === undefined || $scope.namecardForm.address.trim().length === 0) {
			alert("주소를 입력하세요.");
		} else {
			return true;
		}
	};
	// 명함 등록
	$scope.addNamecard = function() {
		if($scope.validation()) {
			Namecards.insert({
				category: $scope.namecardForm.category,
				department: $scope.namecardForm.department,
				position: $scope.namecardForm.position,
				name: $scope.namecardForm.name,
				email: $scope.namecardForm.email,
				phone: $scope.namecardForm.phone,
				address: $scope.namecardForm.address,
				createdAt: new Date(),
				owner: Meteor.userId(),
				username: Meteor.user().username
			});
			// 파라미터로 넘겨서 리스트 페이지로 갔을때 선택되도록 하려고 했으나 실패 ㅜㅜ
			// $state.go("list", {"categoryId":$scope.namecardForm.category});
			location.href = "/list";
		}
	};
	// 명함 수정
	$scope.modifyNamecard = function() {
		if($scope.validation()) {
			Namecards.update($scope.namecardForm.id, {
				$set: {
					category: $scope.namecardForm.category,
					department: $scope.namecardForm.department,
					position: $scope.namecardForm.position,
					name: $scope.namecardForm.name,
					email: $scope.namecardForm.email,
					phone: $scope.namecardForm.phone,
					address: $scope.namecardForm.address
				},
			});
			// 파라미터로 넘겨서 리스트 페이지로 갔을때 선택되도록 하려고 했으나 실패 ㅜㅜ
			// $state.go("list", {"categoryId":$scope.namecardForm.category, "namecardId":$scope.namecardForm._id});
			location.href = "/list";
		}
	};
	// 명함 삭제
	$scope.deleteNamecard = function(id) {
		if(confirm("정말 삭제하시겠습니까??") === true) {
			Namecards.remove({_id: id});
		}

		location.reload();
	};
	// 명함 선택
	$scope.detailNamecard = function(id) {
		$scope.activeNamecard = id;

		//명함id를 방송한다(xs사이즈가 아닐 경우)
		$rootScope.$broadcast('selectNamecard', {namecardId: id});
	}
})

.controller('namecardViewCtrl', function($scope, $rootScope) {
	//방송되는 명함id를 받는다
	$scope.$on('selectNamecard', function(evt, obj) {
		$scope.namecardViews = [];
		// selectNamecard 방송에 보낸 데이터에 따른 분기처리
		// xs사이즈가 아닐때의 처리
		if(obj.namecardId) {
			var namecard = Namecards.findOne({_id: obj.namecardId});
			if(obj.namecardId != undefined) {
				var category = Categorys.findOne({_id:namecard.category});
				// 카테고리 id에 맞는 name 찾아주기
				namecard.categoryName = category.name;
			}
			$scope.namecardViews.push(namecard);
		// xs사이즈일때 select form선택처리
		} else if(obj.categoryId) {
			if(obj.categoryId === '0') {
				var namecards = Namecards.find().fetch();
			} else {
				var namecards = Namecards.find({category: obj.categoryId}).fetch();
			}
			namecards.forEach(function(namecard) {
				var category = Categorys.findOne({_id:namecard.category});
				// 카테고리 id에 맞는 name 찾아주기
				namecard.categoryName = category.name;
				$scope.namecardViews.push(namecard);
				// console.log(namecard);
			})
			console.log(namecards)
		}

	})

	// $scope.deleteNamecard = function(id) {
	// 	if(confirm("정말 삭제하시겠습니까??") === true) {
	// 		console.log(id);
	// 		Namecards.remove({_id: id});

	// 		location.reload();
	// 	}
	// };
})