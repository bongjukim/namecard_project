import { Meteor } from 'meteor/meteor';
import '../imports/api/namecards.js';
import '../imports/api/categorys.js';
import '../imports/api/members.js';

Meteor.startup(() => {
  // code to run on server at startup
});
