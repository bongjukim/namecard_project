import { Mongo } from 'meteor/mongo';
 
export const Members = new Mongo.Collection('members');

if (Meteor.isServer) {
	Meteor.publish('members', function() {
		return Members.find();
	});
}