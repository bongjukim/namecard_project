import { Mongo } from 'meteor/mongo';
 
export const Namecards = new Mongo.Collection('namecards');

if (Meteor.isServer) {
	Meteor.publish('namecards', function() {
		const selector = {
			// when logged in user is the owner
			$and: [{
				owner: this.userId
			}, {
				owner: {
					$exists: true
				}
			}]
		};
 
		return Namecards.find(selector);
	});
}
Namecards.allow({
	insert(userId, namecard) {
		return userId && namecard.owner === userId;
	},
	update(userId, namecard, fields, modifier) {
		return userId && namecard.owner === userId;
	},
	remove(userId, namecard) {
		return userId && namecard.owner === userId;
	}
});
