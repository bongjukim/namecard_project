import { Mongo } from 'meteor/mongo';
 
export const Categorys = new Mongo.Collection('categorys');

if (Meteor.isServer) {
	Meteor.publish('categorys', function() {		
		const selector = {
			// when logged in user is the owner
			$and: [{
				owner: this.userId
			}, {
				owner: {
					$exists: true
				}
			}]
		};
 
		return Categorys.find(selector);
	});
}
Categorys.allow({
	insert(userId, category) {
		return userId && category.owner === userId;
	},
	update(userId, category, fields, modifier) {
		return userId && category.owner === userId;
	},
	remove(userId, category) {
		return userId && category.owner === userId;
	}
});
